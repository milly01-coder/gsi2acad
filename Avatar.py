# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Avatar.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!

import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox


from Crypt import * 

password = b"kamsd12jiaidhkm/scviujhe//iraol2mq823741/oasdjansd0129ioks/mcskndh23l"

class Ui_Dialog(object):
    def setupUi(self, Dialog,index):
        Dialog.setObjectName("Dialog")
        Dialog.setFixedSize(322, 207)
        self.line = QtWidgets.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(10, 70, 301, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")

        self.horizontalLayoutWidget_5 = QtWidgets.QWidget(Dialog)
        self.horizontalLayoutWidget_5.setGeometry(QtCore.QRect(10, 170, 301, 31))
        self.horizontalLayoutWidget_5.setObjectName("horizontalLayoutWidget_5")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_5)
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.pushButton = QtWidgets.QPushButton(self.horizontalLayoutWidget_5)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_5.addWidget(self.pushButton)
        self.pushButton_2 = QtWidgets.QPushButton(self.horizontalLayoutWidget_5)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_5.addWidget(self.pushButton_2)
        self.pushButton_3 = QtWidgets.QPushButton(self.horizontalLayoutWidget_5)
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout_5.addWidget(self.pushButton_3)


        self.pushButton_2.clicked.connect(self.save_encoded)
        self.pushButton_3.clicked.connect(self.del_encoded)
        self.pushButton.clicked.connect(self.del_start)


        self.horizontalLayoutWidget = QtWidgets.QWidget(Dialog)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 90, 301, 22))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.lineEdit = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.horizontalLayoutWidget_2 = QtWidgets.QWidget(Dialog)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(10, 50, 301, 22))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.horizontalLayoutWidget_2)
        self.label_2.setObjectName("label_2")


        self.line_2 = QtWidgets.QFrame(Dialog)
        self.line_2.setGeometry(QtCore.QRect(10, 110, 301, 20))
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")



        self.horizontalLayoutWidget_4 = QtWidgets.QWidget(Dialog)
        self.horizontalLayoutWidget_4.setGeometry(QtCore.QRect(10, 130, 301, 22))
        self.horizontalLayoutWidget_4.setObjectName("horizontalLayoutWidget_4")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_4)
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_4 = QtWidgets.QLabel(self.horizontalLayoutWidget_4)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_4.addWidget(self.label_4)
        self.lineEdit_4 = QtWidgets.QLineEdit(self.horizontalLayoutWidget_4)
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.horizontalLayout_4.addWidget(self.lineEdit_4)



        self.horizontalLayout_2.addWidget(self.label_2)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.horizontalLayoutWidget_2)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout_2.addWidget(self.lineEdit_2)
        self.horizontalLayoutWidget_3 = QtWidgets.QWidget(Dialog)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(10, 10, 301, 22))
        self.horizontalLayoutWidget_3.setObjectName("horizontalLayoutWidget_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(self.horizontalLayoutWidget_3)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.horizontalLayoutWidget_3)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.horizontalLayout_3.addWidget(self.lineEdit_3)


        self.showavatar(index)


        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def del_start(self):
        self.lineEdit_3.setText('')
        self.lineEdit_2.setText('')
        self.lineEdit.setText('')


    def showavatar(self,index):
        print(index)
        f=open("Avatar.txt", "r")
        lines = f.readlines()
        if (len(lines)>0):
            line=lines[index]
            line_split=line[:-1].split('    ')
            self.lineEdit_3.setText(decrypt(password, line_split[0]).decode('utf-8'))
            self.lineEdit_2.setText(decrypt(password, line_split[1]).decode('utf-8'))
            self.lineEdit.setText(decrypt(password, line_split[2]).decode('utf-8'))
        f.close()
        

    def save_encoded(self):
        vorname=self.lineEdit_3.text()
        nachname=self.lineEdit_2.text()
        email=self.lineEdit.text()
        if ((vorname=='') or (nachname=='') or (email=='')):
            box = QMessageBox()
            box.setWindowTitle("Warning")
            box.setIcon(QMessageBox.Warning)
            box.setText(('You forgot one or more Acount Details'),)
            box.setStandardButtons(QMessageBox.Close)
            buttonNo = box.button(QMessageBox.Close)
            if ((vorname=='') and (nachname=='') and (email=='')):
                box.setDetailedText('You forgot your first name, second name and your e-mail')
            
            elif ((vorname=='') and (nachname=='')):
                box.setDetailedText('You forgot your first name and second name')
            elif ((nachname=='') and (email=='')):
                box.setDetailedText('You forgot your second name and e-mail')
            elif ((vorname=='') and (email=='')):
                box.setDetailedText('You forgot your first name and e-mail')

            elif ((vorname=='')):
                box.setDetailedText('You forgot your first name')
            elif ((nachname=='')):
                box.setDetailedText('You forgot your second name')
            elif ((email=='')):
                box.setDetailedText('You forgot your e-mail')
            box.exec_()
        else:
            
            encrypted_vorname = encrypt(password, vorname.encode('utf-8'))
            encrypted_nachname = encrypt(password, nachname.encode('utf-8'))
            encrypted_email = encrypt(password, email.encode('utf-8'))
            strtemp=encrypted_vorname+'    '+encrypted_nachname+'    '+encrypted_email+'\n'
            strtemp_temp=vorname+nachname+email

            f=open("Avatar.txt", "r")
            count=0
            for c, line in enumerate(f, 0):
                line_split=line[:-1].split('    ')
                str_=''
                for d, part in enumerate(line_split, 0):
                    decrypted = decrypt(password, part)
                    str_+=decrypted.decode('utf-8')
                if str_==(strtemp_temp):
                    count=1
            f.close()
            print(count)
            if count==0:
                f=open("Avatar.txt", "a+")
                f.write(encrypted_vorname+'    '+encrypted_nachname+'    '+encrypted_email+'\n')
                f.close()
           
            

    def del_encoded(self):
        vorname=self.lineEdit_3.text()
        nachname=self.lineEdit_2.text()
        email=self.lineEdit.text()
        if ((vorname=='') or (nachname=='') or (email=='')):
            box = QMessageBox()
            box.setWindowTitle("Warning")
            box.setIcon(QMessageBox.Warning)
            box.setText(('You forgot one or more Acount Details'),)
            box.setStandardButtons(QMessageBox.Close)
            buttonNo = box.button(QMessageBox.Close)
            if ((vorname=='') and (nachname=='') and (email=='')):
                box.setDetailedText('You forgot your first name, second name and your e-mail')
            
            elif ((vorname=='') and (nachname=='')):
                box.setDetailedText('You forgot your first name and second name')
            elif ((nachname=='') and (email=='')):
                box.setDetailedText('You forgot your second name and e-mail')
            elif ((vorname=='') and (email=='')):
                box.setDetailedText('You forgot your first name and e-mail')

            elif ((vorname=='')):
                box.setDetailedText('You forgot your first name')
            elif ((nachname=='')):
                box.setDetailedText('You forgot your second name')
            elif ((email=='')):
                box.setDetailedText('You forgot your e-mail')
            box.exec_()
        else:
            f=open("Avatar.txt", "r")
            count=-1
            for c, line in enumerate(f, 0):
                line_split=line[:-1].split('    ')
                str_=''
                for d, part in enumerate(line_split, 0):
                    decrypted = decrypt(password, part)
                    str_+=decrypted.decode('utf-8')
                if str_==(vorname+nachname+email):
                    count=c
            f.close()

            f=open("Avatar.txt", "r")
            lines = f.readlines()
            f.close()

            print(count)
            if count>=0:
                del lines[count]
           
            new_file = open("Avatar.txt", "w+")
            for line in lines:
                new_file.write(line)
            new_file.close()
            

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "e-Mai :        "))
        self.label_2.setText(_translate("Dialog", "Nachname :"))
        self.label_3.setText(_translate("Dialog", "Vorname :  "))
        self.label_4.setText(_translate("Dialog", "Licens :       "))
        self.pushButton.setText(_translate("Dialog", "New"))
        self.pushButton_2.setText(_translate("Dialog", "Save"))
        self.pushButton_3.setText(_translate("Dialog", "Delete"))


#if __name__ == "__main__":
#    app = QtWidgets.QApplication(sys.argv)
#    Dialog = QtWidgets.QDialog()
#    ui = Ui_Dialog()
#    ui.setupUi(Dialog,0)
#    Dialog.show()
#    app.exec_()


#decrypted = decrypt(password, "4EaNdkUN21ReBsUGh2+OTv7Kur3shXBupZ4OR0xdFBc=btJq/y8Zj+k/KLMD1s0p2cf86z366PcI6ZHDTAPaETY=")
#print(decrypted)

