
import sys
from PyQt5.QtCore import Qt,QDir
from PyQt5 import QtWidgets,QtCore, QtGui 


### Numpy, matplotlib, basemap
import numpy as np
#from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas

from PyQt5.QtWidgets import QMessageBox


import time

laengeneinehit=[10**-3,1/3.28084*10**-3,None,None,None,None,10**-4,1/3.28084*10**-4,10**-5]
password = b"kamsd12jiaidhkm/scviujhe//iraol2mq823741/oasdjansd0129ioks/mcskndh23l"

from Avatar import * 
from Icon_Gender import * 
from Get_Current_Index import *


class Point_3D():
    def __init__(self,Name,Ost=None,Nord=None,H=None,Info=None):
        self.__setName(Name)
        self.__setOst(Ost)
        self.__setNord(Nord)
        self.__setH(H)
        self.__setInfo(Info)
    
    def __getName(self):
        return self.__Name
    def __setName(self,Name):
        self.__Name=Name     
    Name=property(__getName, __setName) 

    def __getOst(self):
        return self.__Ost
    def __setOst(self,Ost):
        self.__Ost=Ost     
    Ost=property(__getOst, __setOst) 

    def __getNord(self):
        return self.__Nord
    def __setNord(self,Nord):
        self.__Nord=Nord     
    Nord=property(__getNord, __setNord) 

    def __getH(self):
        return self.__H
    def __setH(self,H):
        self.__H=H     
    H=property(__getH, __setH) 

    def __getInfo(self):
        return self.__Info
    def __setInfo(self,Info):
        self.__Info=Info     
    Info=property(__getInfo, __setInfo) 




class GUI(QtWidgets.QMainWindow):
    ### This class is initialized here.
    def __init__(self, parent=None):
        """ This gets called everytime an instance of this class is created."""
        super(GUI, self).__init__(parent)
        
        self.filename=None
        self.Daten=[]
        self.setWindowTitle('Temp')
        self.setFixedSize(1199, 748)
        self.createMenu()
        self.createMainFrame()
        self.createStatusBar()
        self.show()

    def createMainFrame(self):

        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 80, 1181, 281))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")


        
        self.NamenBild = QtWidgets.QPushButton(self.centralwidget)
        self.NamenBild.setGeometry(QtCore.QRect(1140, 0, 51, 41))
        self.NamenBild.setObjectName("NamenBild")
        icon  = QtGui.QPixmap('avatar_icon.png')
        self.NamenBild.setIcon(QtGui.QIcon(icon))
        self.NamenBild.setIconSize(QtCore.QSize(40,40))
        self.NamenBild.clicked.connect(self.set_Avatar)
        
        
        
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(910, 10, 221, 22))
        self.comboBox.setObjectName("comboBox")
        
        f=open("Avatar.txt", "r")
        count=0
        for c, line in enumerate(f, 0):
            line_split=line[:-1].split('    ')
            if line_split[0]!='':
                decrypted = decrypt(password, line_split[2])
                str_=decrypted.decode('utf-8')
                self.comboBox.addItem(str_)  
        f.close()
        self.switchavatar()
        self.comboBox.currentIndexChanged.connect(self.switchavatar)


        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 0, 371, 41))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.Import = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.Import.setObjectName("Import")
        self.horizontalLayout.addWidget(self.Import)
        self.Import.clicked.connect(self.showData)

        self.Berechnen = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.Berechnen.setObjectName("Berechnen")
        self.horizontalLayout.addWidget(self.Berechnen)
        self.Export = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.Export.setObjectName("Export")
        self.horizontalLayout.addWidget(self.Export)


        self.Export.clicked.connect(self.exportData)
        

        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(10, 40, 1181, 20))
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")

        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath('')
        self.model.setNameFilters(['*.GSI'])
        self.model.setNameFilterDisables(0)

        self.treeView = QtWidgets.QTreeView(self.gridLayoutWidget)
        self.treeView.setObjectName("treeView")
        self.treeView.setModel(self.model)
        self.treeView.setAnimated(False)
        self.treeView.setIndentation(20)
        self.treeView.setSortingEnabled(True)
        self.treeView.setColumnWidth(0,250)
        self.treeView.setColumnWidth(1,50)
        self.treeView.setColumnWidth(2,100)
        self.treeView.setColumnWidth(3,100)        
        self.treeView.setWindowTitle("Dir View")
        self.treeView.resize(640, 640)
        self.treeView.doubleClicked.connect(self.handleSelection)


        self.gridLayout.addWidget(self.treeView, 0, 0, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 0, 1, 1, 1)
        self.pushButton.clicked.connect(self.showData)

        self.treeView_2 = QtWidgets.QTreeWidget(self.gridLayoutWidget)
        self.treeView_2.setObjectName("treeView_2")
        header=QtWidgets.QTreeWidgetItem(['Name', 'Info', 'Ost [m]', 'Nord [m]','H [m]'])
        self.treeView_2.setHeaderItem(header)
        self.treeView_2.setColumnWidth(0,100)
        self.treeView_2.setColumnWidth(1,100)
        self.treeView_2.setColumnWidth(2,100)
        self.treeView_2.setColumnWidth(3,100)
        self.treeView_2.setColumnWidth(4,100)
        self.gridLayout.addWidget(self.treeView_2, 0, 2, 1, 1)
        


        self.formLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.formLayoutWidget.setGeometry(QtCore.QRect(10, 380, 1181, 321))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setHorizontalSpacing(70)
        self.formLayout.setObjectName("formLayout")


        self.infoBrowser = QtWidgets.QTextBrowser(self.formLayoutWidget)
        self.infoBrowser.setObjectName("treeWidget")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.infoBrowser)

        self.textBrowser = QtWidgets.QTextBrowser(self.formLayoutWidget)
        self.textBrowser.setObjectName("textBrowser")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.textBrowser)
        

        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(10, 360, 1181, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
    
        self.formLayoutWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.formLayoutWidget_2.setGeometry(QtCore.QRect(10, 60, 461, 21))
        self.formLayoutWidget_2.setObjectName("formLayoutWidget_2")
        self.formLayout_2 = QtWidgets.QFormLayout(self.formLayoutWidget_2)
        self.formLayout_2.setContentsMargins(0, 0, 0, 0)
        self.formLayout_2.setObjectName("formLayout_2")
        self.label = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label.setMidLineWidth(0)
        self.label.setObjectName("label")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.label_2 = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.label_2.setLineWidth(1)
        self.label_2.setMidLineWidth(0)
        self.label_2.setObjectName("label_2")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.label_2)


        self.setCentralWidget(self.centralwidget)

        self.retranslateUi()
    
    def switchavatar(self):
        png=witch(self.comboBox.currentText())
        icon  = QtGui.QPixmap(png)
        self.NamenBild.setIcon(QtGui.QIcon(icon))
        self.NamenBild.setIconSize(QtCore.QSize(40,40))
        

    def set_Avatar(self):
        temp=self.comboBox.currentText()
        index=get_index(self.comboBox.currentText())
        dialog = QtWidgets.QDialog()
        ui =  Ui_Dialog()
        ui.setupUi(dialog,index)
        dialog.exec_()
        self.comboBox.clear()
        f=open("Avatar.txt", "r")
        count=0
        for c, line in enumerate(f, 0):
            line_split=line[:-1].split('    ')
            decrypted = decrypt(password, line_split[2])
            str_=decrypted.decode('utf-8')
            self.comboBox.addItem(str_)  
        f.close()
        self.comboBox.setCurrentIndex(count)

    
    def createStatusBar(self):
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.showMessage('Ready')
        self.setStatusBar(self.statusbar)
        self.statusBar().reformat()
        self.statusBar().setStyleSheet('border: 0; color:  black;')
        

    def createMenu(self):
        self.menubar = QtWidgets.QMenuBar(self)
        self.menubar.setNativeMenuBar(False)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1126, 26))
        
        self.menubar.setObjectName("Exit")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("Exit")

        openfile = QtWidgets.QAction('&Open File', self)        
        openfile.setShortcut('Ctrl+O')
        openfile.setStatusTip('Open File')
        openfile.triggered.connect(self.file_open)

        exitAction = QtWidgets.QAction('&Exit', self)        
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.exitCall)#

        self.menubar.addAction(openfile)
        self.menubar.addAction(exitAction)
        
        self.setMenuBar(self.menubar)

    #Show Data in Viewtree like SP->ZP->HZ/VZ/S
    def showData(self):
        
        def GSI_2_M(parts):
            parts=sequenz.split('+')
            einheit=int(parts[0][-1])
            zahl=int(parts[1])*laengeneinehit[einheit]
            return zahl

        if self.filename==None or self.filename[-4:]!='.GSI':
            self.statusbar.showMessage("Warning")
            box = QMessageBox()
            box.setWindowTitle("Warning")
            box.setIcon(QMessageBox.Warning)
            box.setText(('There is no filepath or no .gsi selected .\n ' +
             'Select "Ignore" to stay in th gui \n or "Close" to exit the program \n'),)
            box.setStandardButtons(QMessageBox.Close)
            box.setDefaultButton(QMessageBox.Close)
            buttonNo = box.button(QMessageBox.Close)
            box.setDetailedText('You can choose the file path with den Tree View or with the meue folder "Open File".\nOr you have not choose a Leica .gis file')
            box.exec_()
            if box.clickedButton() == buttonNo: 
                box.close() 
                self.statusbar.showMessage("Ready")
                

        else:
            self.infoBrowser.append('read Data:       '+self.filename)
            self.treeView_2.clear() 
            f = open(self.filename, "r")
            self.Daten=[]
            SP=[]
            ZP=[]
            for line in f:
                line_sequenz=line[1:].split(' ')
                SP_bool=False
                name_new=''
                info_new=' '
                Ost_number=None
                Nord_number=None
                H_number=None
                E0_number=None
                N0_number=None
                H0_number=None
                ZPH_number=None
                IH_number=None

                for sequenz in line_sequenz[:-1]:      
                    code = int(sequenz[:2])

                    if code==11:
                        name_part=sequenz.split('+')
                        for c, value in enumerate(name_part[1], 0):
                            if value!='0':
                                name_new = name_part[1][c:]
                                break     
                    elif code==21:
                        continue

                    elif code==71:
                        info_part=sequenz.split('+')
                        for c, value in enumerate(info_part[1], 0):
                            if value!='0':
                                info_new = info_part[1][c:]  
                                break     

                    elif code==81:
                        Ost_number = round(GSI_2_M(sequenz),3)         

                    elif code==82:
                        Nord_number = round(GSI_2_M(sequenz),3)  
             
                    elif code==83:
                        H_number = round(GSI_2_M(sequenz),3)      

                    elif code==84:
                        self.Daten.append([SP,ZP])
                        SP_bool=True
                        SP=[]
                        ZP=[]
                        E0_number = round(GSI_2_M(sequenz),3)  

                    elif code==85:
                        N0_number = round(GSI_2_M(sequenz),3)   

                    elif code==86:
                        H0_number = round(GSI_2_M(sequenz),3)    

                    elif code==87:
                         IH_number = round(GSI_2_M(sequenz),3)  

                    elif code==88:
                        ZPH_number = round(GSI_2_M(sequenz),3)  
                    else:
                        continue

                if SP_bool==False:
                    ZP.append(Point_3D(name_new,Ost_number,Nord_number,H_number,info_new))
                elif SP_bool==True:
                    SP.append(Point_3D('SP_'+name_new,E0_number,N0_number,H0_number,'Standpunkt'))
            self.Daten.append([SP,ZP])
            self.Daten.pop(0)
            f.close() 

            
            for i in range (0,len(self.Daten)):
                parent1 = QtWidgets.QTreeWidgetItem(self.treeView_2,[str(self.Daten[i][0][0].Name),str(self.Daten[i][0][0].Info),str(self.Daten[i][0][0].Ost),str(self.Daten[i][0][0].Nord),str(self.Daten[i][0][0].H)])
                for j in range(0,len(self.Daten[i][1])):
                     chl1=QtWidgets.QTreeWidgetItem(parent1,[str(self.Daten[i][1][j].Name),str(self.Daten[i][1][j].Info),str(self.Daten[i][1][j].Ost),str(self.Daten[i][1][j].Nord),str(self.Daten[i][1][j].H)])

            self.treeView_2.show()


    #Read Filename with the Viewtree
    def handleSelection(self):

        indexes = self.treeView.selectedIndexes()
        self.filename=indexes[0].data()
        knote=indexes[0]
        bedingung=True
        while bedingung:
            part=knote.parent().data()
            if part!= None: 
                lastpart=part[-1]
                if lastpart!=")":
                    self.filename=part+"/"+self.filename
                else:
                    self.filename=part[-3:-1]+"/"+self.filename
            else:
                bedingung=False
            knote=knote.parent()
        
        #print(self.filename)
    

    #Read Filename with the Meue Open File
    def file_open(self):
        name = QtWidgets.QFileDialog.getOpenFileName(self,'Open File',filter='*.GSI')
        self.filename=name[0]
        split_=self.filename.split('/')
        unfold = len(self.filename.split('/'))-1

        for i in range (1,unfold+1):
            name = ''
            for t in split_[:i]:
                name+=t+'/'
            root_index =self.model.index(name)
            self.treeView.expand(root_index)

        self.treeView.show()
                                       
        self.infoBrowser.append('selected Data:   '+self.filename)
        

    def exportData(self):
        if len(self.Daten)>0:
            #datename=self.filename.split('/')[-1][:-4]+'.scr'
            datename=self.filename[:-4]+'.scr'
            file = open(datename, "w") 
            file.write(";Skript from App for AutoCad\n")
            for i in range (0,len(self.Daten)):
                for j in range(0,len(self.Daten[i][1])):
                        file.write("_POINT "+str(self.Daten[i][1][j].Ost)+","+str(self.Daten[i][1][j].Nord)+","+str(self.Daten[i][1][j].H)+"\n")
                        file.write("_CIRCLE "+str(self.Daten[i][1][j].Ost)+","+str(self.Daten[i][1][j].Nord)+" 1\n")
                        file.write("_TEXT "+str(self.Daten[i][1][j].Ost)+","+str(self.Daten[i][1][j].Nord)+","+str(self.Daten[i][1][j].H)+" 0.5 0 "+str(self.Daten[i][1][j].Name)+"_"+str(self.Daten[i][1][j].Info)+"\n")
            file.write("_zoom _e\n")
            file.write(";_qsave\n")
            file.close() 
            self.textBrowser.clear()
            file = open(datename, "r") 
            f=file.readlines()
            for line in f:
                self.textBrowser.append(line)
            file.close()    
            self.infoBrowser.append('export Data:     '+datename)
            
        else:
            print("NO Data")




    def exitCall(self):
        sys.exit(0)  

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.pushButton.setText(_translate("MainWindow", "-->"))
        self.menuFile.setTitle(_translate("MainWindow", "Exit"))
        self.Import.setText(_translate("MainWindow", "Import"))
        self.Berechnen.setText(_translate("MainWindow", "Berechnen"))
        self.Export.setText(_translate("MainWindow", "Export"))
        self.NamenBild.setText(_translate("MainWindow", ""))
        self.label.setText(_translate("MainWindow", "Import Datei: "))
        self.label_2.setText(_translate("MainWindow", ""))



### Application and example objects are created
if __name__ == '__main__':
    ### Instantiate a Qt application
    app = 0
    app = QtWidgets.QApplication(sys.argv)

    ### Add icon
    app.setWindowIcon(QtGui.QIcon('Icon_file3.ico'))

    ### Add our custom example to the application
    form = GUI()
    ### Start the application
    app.exec_()
